<?php

/**
 * Plugin Name:       GitHub Updater TEST
 * Plugin URI:        https://bitbucket.org/luboslopour/github-updater-test
 * Description:       Bla blaWordPress.
 * Version:           1.0.2
 * Author:            Luboš Lopour
 * License:           UNLICENCED
 * Domain Path:       /languages
 * Text Domain:       github-updater-test
 * Bitbucket Plugin URI: https://bitbucket.org/luboslopour/github-updater-test
 */